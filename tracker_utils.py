import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN

# set trackreconstruction software in sys path
import os, sys
script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_dir, 'trackreconstruction'))

# disable tqdm in trackreconstruction software
from tqdm import tqdm
from functools import partialmethod
tqdm.__init__ = partialmethod(tqdm.__init__, disable=True)

from trackreconstruction import config_run
from trackreconstruction.fitting import *

staves     = 3
dist_z     = config_run.dist_z
chip_nx    = config_run.chip_nx
chip_ny    = config_run.chip_ny
pitch_xy   = config_run.pitch_xy
DBSCAN_eps = config_run.DBSCAN_eps
man_mask_flag = config_run.man_mask_flag
pix_mask      = config_run.pix_mask


def index_to_chipid(index):
    stave_id = index//10

    chip_id_lsb = index%10
    if (chip_id_lsb > 4):
        chip_id_lsb += 3

    chip_id = 0x70 | (stave_id<<8) | chip_id_lsb
    return chip_id


map_chip_pos = pd.DataFrame({
    'chip_id' : [index_to_chipid(x) for x in range(staves*10)],
    'i_x' : [0,1,2,3,4,4,3,2,1,0]*staves,
    'i_y' : [1,1,1,1,1,0,0,0,0,0]*staves,
    'i_z' : [x//10 for x in range(30)]
    })



def cluster_ev(ev):
    X = ev[['x_pos','y_pos','z_pos']].values
    clustering = DBSCAN(eps=DBSCAN_eps, min_samples=1, metric='euclidean')
    clustering.fit(X)
    return np.array(clustering.labels_)


def clustering_algo(df_final_aug):
    df_final_aug['cls_nr'] = -1*np.ones(len(df_final_aug))

    cls_id = []
    cls_size = []
    x_centroid = []
    y_centroid = []
    z_centroid = []
    trk_index = []

    index = df_final_aug.index.values
    clusters = cluster_ev(df_final_aug)
    df_final_aug.loc[index, ['cls_nr']] = clusters

    for i_cls in np.unique(df_final_aug.cls_nr.values):
        df_cls_ev = df_final_aug[df_final_aug.cls_nr == i_cls]
        cls_id.append(i_cls)
        cls_size.append(len(df_cls_ev))
        x_centroid.append(df_cls_ev.x_pos.mean())
        y_centroid.append(df_cls_ev.y_pos.mean())
        z_centroid.append(df_cls_ev.z_pos.mean())

        df_centroids = pd.DataFrame({'cluster' : cls_id,'cls_size' : cls_size,
            'x_pos' : x_centroid, 'y_pos' : y_centroid, 'z_pos' : z_centroid
            })

    return df_centroids



def fit_clusters(df_clusters):
    triple_ev = df_clusters.groupby(by=['event'])['z_pos'].nunique()[df_clusters.groupby(by=['event'])['z_pos'].nunique()==3].index.values

    df_clusters = df_clusters[df_clusters.event.isin(triple_ev)]

    if len(df_clusters) == 0:
        return [], [], [], [], [], []

    #print('Hough Track Seeding running...')
    df_clusters_hough_xy = HoughTransform_xy(df_clusters)
    df_clusters_hough_xy['event_trk_nr'] = df_clusters_hough_xy[['event','trk_nr']].apply(lambda x: str(x[0])+'_'+str(x[1]), axis=1)
    horizontal_xy = df_clusters_hough_xy.groupby(by=['event_trk_nr'])['z_pos'].nunique()[df_clusters_hough_xy.groupby(by=['event_trk_nr'])['z_pos'].nunique()<2].index.values
    df_clusters_hough_xy["trk_nr"] = df_clusters_hough_xy[['event_trk_nr','trk_nr']].apply(lambda x: -1 if x[0] in horizontal_xy else x[1], axis=1)

    df_clusters_hough_xz = HoughTransform_xz(df_clusters)
    df_clusters_hough_xz['event_trk_nr'] = df_clusters_hough_xz[['event','trk_nr']].apply(lambda x: str(x[0])+'_'+str(x[1]), axis=1)
    horizontal_xz = df_clusters_hough_xz.groupby(by=['event_trk_nr'])['z_pos'].nunique()[df_clusters_hough_xz.groupby(by=['event_trk_nr'])['z_pos'].nunique()<2].index.values
    df_clusters_hough_xz["trk_nr"] = df_clusters_hough_xz[['event_trk_nr','trk_nr']].apply(lambda x: -1 if x[0] in horizontal_xz else x[1], axis=1)

    df_clusters_hough_yz = HoughTransform_yz(df_clusters)
    df_clusters_hough_yz['event_trk_nr'] = df_clusters_hough_yz[['event','trk_nr']].apply(lambda x: str(x[0])+'_'+str(x[1]), axis=1)
    horizontal_yz = df_clusters_hough_yz.groupby(by=['event_trk_nr'])['z_pos'].nunique()[df_clusters_hough_yz.groupby(by=['event_trk_nr'])['z_pos'].nunique()<2].index.values
    df_clusters_hough_yz["trk_nr"] = df_clusters_hough_yz[['event_trk_nr','trk_nr']].apply(lambda x: -1 if x[0] in horizontal_yz else x[1], axis=1)

    #print('Choose best hough')
    df_clusters_hough_best, plane_choosen = ChooseBestHough(df_clusters_hough_xy,df_clusters_hough_xz,df_clusters_hough_yz)

    #print('Fitting step running...')
    ev_n, direct, point, theta, phi, cls_size = fit_all_events(df_clusters_hough_best, "final", plane_choosen)

    return (ev_n, direct, point, theta, phi, cls_size)



def cal_pix_position(event_data):

    def reflex_y(row):
        if row['i_y'] == 0:
            return (chip_ny-1) - row['y']
        else:
            return row['y']


    def reflex_x(row):
        if row['i_y'] == 0:
            return (chip_nx-1) - row['x']
        else:
            return row['x']

    if man_mask_flag == True and len(pix_mask) != 0:
        event_data = event_data[np.all(event_data != np.array(pix_mask), axis=1)]

    df_sel = pd.DataFrame(event_data, columns = ['chip_id', 'x', 'y'])

    df_final_aug = pd.merge(map_chip_pos, df_sel, on = 'chip_id')
    df_final_aug['x_new'] = df_final_aug.apply(reflex_x, axis = 1)
    df_final_aug['y_new'] = df_final_aug.apply(reflex_y, axis = 1)

    df_final_aug['x_pos'] = df_final_aug.apply(
            lambda x: (x['i_x'] * chip_nx + x['x_new']) * pitch_xy, axis=1)
    df_final_aug['y_pos'] = df_final_aug.apply(
            lambda x: (x['i_y'] * chip_ny + x['y_new']) * pitch_xy, axis=1)
    df_final_aug['z_pos'] = df_final_aug.apply(
            lambda x: (-x['i_z'] * dist_z), axis=1)

    df_final_aug = clustering_algo(df_final_aug)
    df_final_aug['event'] = np.zeros(len(df_final_aug))
    _, direct, point, theta, phi, cls_size = fit_clusters(df_final_aug)

    return df_final_aug[['x_pos', 'y_pos', 'z_pos']].to_numpy(), direct, point
