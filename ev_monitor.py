import os
import ctypes as cty
import numpy as np
import datetime
import argparse

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtWidgets
from time import sleep

import tracker_utils

# ------------------------------------------------------------------------------------
# File handling
# ------------------------------------------------------------------------------------

class file_reader():

    def __init__(self, fname, header_dict, skip_n=0):
        self.binfile = open(fname, 'rb')
        self.binfile.seek(skip_n)

        if (header_dict['hdr_size_type'] not in ('full', 'payload')):
            raise ValueError('hdr_size_type must be "full" or "payload"')

        size_highaddr = header_dict['pkt_size_offset'] + header_dict['pkt_size_len']
        self.hdr_dict = {
                'size' : header_dict['size'],
                'pkt_size_offset' : header_dict['pkt_size_offset'],
                'pkt_size_len'    : header_dict['pkt_size_len'],
                'pkt_size_hi'     : size_highaddr,
                'hdr_size_type'   : header_dict['hdr_size_type']
                }

        self.packet_count = 0


    def read_chunk(self, size):
        data = bytearray()

        while size != 0:
            rd_data = self.binfile.read(size)
            if len(rd_data) == 0:
                sleep(0.1)
                continue

            data += rd_data
            size -= len(rd_data)

        return data


    def read_packet(self):
        data = self.read_chunk(self.hdr_dict['size'])

        pkt_size = data[self.hdr_dict['pkt_size_offset'] : self.hdr_dict['pkt_size_hi']]
        pkt_size = int.from_bytes(pkt_size, byteorder="little")

        if self.hdr_dict['hdr_size_type'] == 'full':
            pkt_size -= self.hdr_dict['size']

        pkt_data = self.read_chunk(pkt_size)
        self.packet_count += 1

        data += pkt_data
        return bytes(data)


    def skip_packets(self, n):
        for i in range(n):
            data = self.read_chunk(self.hdr_dict['size'])

            pkt_size = data[self.hdr_dict['pkt_size_offset'] : self.hdr_dict['pkt_size_hi']]
            pkt_size = int.from_bytes(pkt_size, byteorder="little")

            if self.hdr_dict['hdr_size_type'] == 'full':
                pkt_size -= self.hdr_dict['size']

            self.binfile.seek(pkt_size, 1)
            self.packet_count += 1



class tracker_raw_reader(file_reader):


    def __init__(self, filename='./data/dout.raw'):
        hdr_dict = {
                'size' : 12,
                'pkt_size_offset' : 2,
                'pkt_size_len'    : 2,
                'hdr_size_type' : 'payload'
                }

        super(tracker_raw_reader, self).__init__(filename, header_dict=hdr_dict, skip_n=4)

        # data decoder
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.decoder = cty.cdll.LoadLibrary(script_dir + '/process_raw.so')
        self.decoder.decode_raw.argtypes = [cty.c_char_p, cty.c_int, cty.c_char_p]

        self.decoder.get_last_event.argtypes = [np.ctypeslib.ndpointer(cty.c_uint16,
            flags="C_CONTIGUOUS")]



    def get_dec_event(self):
        # get event packet and decode
        data = self.read_packet()
        self.decoder.decode_raw(data, cty.c_int(len(data)), None)

        # get event data from decoder (chip_id, x ,y)
        MAX_HITS = 4000
        buff = np.zeros(3*MAX_HITS, dtype=np.uint16)
        hitcnt = self.decoder.get_last_event(buff)

        buff = buff.reshape(MAX_HITS, 3)
        return buff[:hitcnt]


    def get_event(self):
        data = self.get_dec_event()
        if len(data) == 0:
            return np.array([])

        final_data = tracker_utils.cal_pix_position(data)
        return final_data



class digitizer_reader():


    def __init__(self, channels, base_dir='./data/'):

        self.hdr_dict = {
                'size' : 24,
                'pkt_size_offset' : 0,
                'pkt_size_len'    : 4,
                'hdr_size_type' : 'full'
                }

        self.ch_reader = []
        for ch in range(channels):
            bin_path = os.path.join(base_dir, f'wave_{ch}.dat')
            self.ch_reader.append(file_reader(bin_path, header_dict=self.hdr_dict))



    def get_event(self):
        data = []

        for ch in self.ch_reader:
            ch_data = ch.read_packet()

            # remove header
            ch_data = ch_data[self.hdr_dict['size']:]
            data.append(np.frombuffer(ch_data, dtype=np.dtype('f')))

        return data


    def skip_packets(self, n):
        for ch in self.ch_reader:
            ch.skip_packets(n)



# ------------------------------------------------------------------------------------
# Plotting
# ------------------------------------------------------------------------------------


class datawatcher(QtCore.QThread):

    data_avaiable = QtCore.pyqtSignal(int, object, object)

    def __init__(self, digitizer_channels=4, min_interval_ms=150):
        super(datawatcher, self).__init__()

        self.digi_reader = digitizer_reader(digitizer_channels, base_dir=args.digi_file)

        if args.tkr_file == None:
            self.tracker_reader = None
        else:
            self.tracker_reader = tracker_raw_reader(filename=args.tkr_file)

        self.min_interval = min_interval_ms * 1000
        self.t_last_update = datetime.datetime.now()


    def run(self):
        while True:
            time_now = datetime.datetime.now()

            if (time_now - self.t_last_update).microseconds > self.min_interval:
                digi_data = self.digi_reader.get_event()

                if self.tracker_reader != None:
                    trk_data = self.tracker_reader.get_event()
                else:
                    trk_data = np.array([])


                event_cnt = self.digi_reader.ch_reader[0].packet_count

                self.data_avaiable.emit(event_cnt, digi_data, trk_data)
                self.t_last_update = time_now

            else:
                self.digi_reader.skip_packets(1)

                if self.tracker_reader != None:
                    self.tracker_reader.skip_packets(1)



class digitizer_plotter():

    def __init__(self, win):
        DIGITIZER_CHANNELS = 4

        digi_plot = win.addPlot(title="<font color=#0066cc>Digitizer</font>",
                row=0, col=0, rowspan=1)
        digi_plot.disableAutoRange()
        digi_plot.setRange(xRange=(0,1200), yRange=(0,1200))
        digi_plot.addLegend()

        self.curve = []
        for i in range(DIGITIZER_CHANNELS):
            self.curve.append(digi_plot.plot(name=f'ch{i}', pen=(i,DIGITIZER_CHANNELS)))

        digi_histo_plot = win.addPlot(title="<font color=#0066cc>Max Histo</font>",
                row=1, col=0, rowspan=1)

        self.digi_histo_curve = []
        for i in range(DIGITIZER_CHANNELS):
            self.digi_histo_curve.append(digi_histo_plot.plot(stepMode="center",
                fillLevel=0, fillOutline=False, pen=(i,DIGITIZER_CHANNELS)))

        self.histo_vals = [np.array([])] * DIGITIZER_CHANNELS

        self.evcnt_label = pg.TextItem(border='blue')
        digi_plot.addItem(self.evcnt_label)


    def update(self, event_count, digitizer_data):
        self.evcnt_label.setText(f'Event Count: {event_count}')

        # update digitizer plot
        for i in range(len(digitizer_data)):
            self.curve[i].setData(digitizer_data[i])

            self.histo_vals[i] = np.append(self.histo_vals[i], np.max(digitizer_data[i]))
            y, x = np.histogram(self.histo_vals[i], bins=50)
            self.digi_histo_curve[i].setData(x, y)



class tracker_plotter():

    def __init__(self, win):
        planes = ['xy', 'xz', 'yz']

        self.tracker_plot = []
        for i in range(3):
            title = '<font color=#0066cc> Tracker ' + planes[i] + '</font>'
            labels = {'left' : planes[i][1] + ' (mm)',
                    'bottom' : planes[i][0] + ' (mm)'}
            self.tracker_plot.append(win.addPlot(title=title, row=i, col=1, labels=labels))

        self.__draw_tracker_grid()

        self.track_histo_val = np.zeros(20)
        labels = {'left' : 'Count', 'bottom' : 'Number of tracks'}
        track_histo_plot = win.addPlot(title="<font color=#0066cc>Tracks histo</font>", labels=labels, row=2, col=0)
        self.track_histo_curve = track_histo_plot.plot(brush='blue', stepMode="center",
                fillLevel=0, fillOutline=False)

        self.stacked_view = QtWidgets.QCheckBox('Stacked View')
        act = QtWidgets.QWidgetAction(self.tracker_plot[0].getViewBox().menu)
        act.setDefaultWidget(self.stacked_view)
        self.tracker_plot[0].getViewBox().menu.addAction(act)


    def __draw_tracker_grid(self):
        # x,y rect
        self.tracker_plot[0].plot(np.array([[0,0], [150,0], [150,30], [0,30], [0,0]]))
        # x,z and y,z lines
        for i in range(3):
            dist_z = tracker_utils.dist_z
            self.tracker_plot[1].plot(np.array([[0, -i*dist_z], [150, -i*dist_z]]))
            self.tracker_plot[2].plot(np.array([[0, -i*dist_z], [30, -i*dist_z]]))

        # axis ranges size
        self.tracker_plot[0].setRange(xRange=(-20,160), yRange=(-20,40))
        self.tracker_plot[1].setRange(xRange=(-20,160), yRange=(-30,15))
        self.tracker_plot[2].setRange(xRange=(-60,100), yRange=(-30,10))


    def __get_line(self, direction, point, l, m):
        pt_x = np.array([0, 150])
        pt_y = (point[l]-point[m]*direction[l]/direction[m]) + direction[l]/direction[m] * pt_x

        return pt_x, pt_y


    def update(self, tracker_data):

        if len(tracker_data) == 0:
            return

        data_points = tracker_data[0]
        direction   = tracker_data[1]
        point       = tracker_data[2]

        # update histogram
        hbin = np.amin([len(self.track_histo_val)-1, len(direction)])
        self.track_histo_val[hbin] += 1
        self.track_histo_curve.setData(np.arange(len(self.track_histo_val)+1), self.track_histo_val)

        # clear plot and redraw grid
        if not self.stacked_view.isChecked():
            for plane in range(3):
                self.tracker_plot[plane].clear()

            self.__draw_tracker_grid()

        # plot points
        self.tracker_plot[0].plot(data_points[:, 0:2],   symbol='x', pen=None) # xy
        self.tracker_plot[1].plot(data_points[:, [0,2]], symbol='x', pen=None) # xz
        self.tracker_plot[2].plot(data_points[:, 1:3],   symbol='x', pen=None) # xz

        if self.stacked_view.isChecked():
            return

        # plot lines
        planes = [[1,0], [2,0], [2,1]]
        p = 0

        for x1, x2 in planes:
            #plot new tracks
            for i in range(len(direction)):
                pt_x, pt_y = self.__get_line(direction[i], point[i], x1, x2)
                self.tracker_plot[p].plot(pt_x, pt_y, pen=(i, len(direction)))
            p += 1




if __name__ == '__main__':

    def str_to_int(x):
        return int(x, 0)

    parser = argparse.ArgumentParser()

    parser.add_argument('--file-tkr', type=str, dest='tkr_file', required=False,
            help='digitizer folder')
    parser.add_argument('--file-digi', type=str, dest='digi_file', required=True,
            help='tracker .raw file')
    parser.add_argument('-r', type=str_to_int, dest='rate', required=False, default=150,
            help='max refresh rate')

    args = parser.parse_args()


    win = pg.GraphicsLayoutWidget(show=True, title='Event monitor', size=(1920,1080))

    digi_plotter = digitizer_plotter(win)
    trk_plotter = tracker_plotter(win)


    def update_plots(event_count, digitizer_data, tracker_data):

        digi_plotter.update(event_count, digitizer_data)
        trk_plotter.update(tracker_data)


    dw = datawatcher(min_interval_ms=args.rate)
    dw.data_avaiable.connect(update_plots)
    dw.start()

    pg.exec()
